﻿using System;

namespace Authentication
{
    public class RandomCode
    {
        public static int CodeAuthorization()
        {
            var random = new Random();
            return random.Next(999, 10000);
        }
    }
}
