﻿using System;
using Authentication;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;

namespace TwoFactorAuthentication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Добро пожаловать в главное меню: \n1. Регистрация;\n0. Выход;\nВыбор: ");
            var choice=Console.ReadLine();
            switch (choice)
            {
                case "1":
                    var comparison = new DataComparison();
                    Console.Clear();
                    Console.Write("Для регистрации введите Ваш номер телефона (формат ввода: 7XXXXXXXXXX): ");
                    string phoneNumber = Console.ReadLine();
                    int randomCode = RandomCode.CodeAuthorization();
                    const string accountSid = "ACaddac989f2f1e947d2615d5b598719a2";
                    const string authToken = "cbc41ca9b42be97590c9f8647ca5d49f";
                    TwilioClient.Init(accountSid, authToken);
                    try
                    {
                        var message = MessageResource.Create(
                            body: $"Ваш код подтверждения: {randomCode}.",
                            from: new Twilio.Types.PhoneNumber("+19287702279"),
                            to: new Twilio.Types.PhoneNumber($"+{phoneNumber}")
                        );

                        // Сообщение с информацией об отправке смс
                        //Console.WriteLine(message.Sid);

                        int NumberOfAttempts = 1;
                        while (true)
                        {
                            Console.Write("Введите код из СМС: ");
                            int userValue;
                            if (int.TryParse(Console.ReadLine(), out userValue)) { }
                            if (comparison.Comparison(randomCode, userValue) == true)
                            {
                                Console.WriteLine("Вы успешно зарегистрированы!");
                                break;
                            }
                            else if (NumberOfAttempts == 3)
                            {
                                Console.Clear();
                                Console.WriteLine("Колличество попыток истекло, попробуйте позднее!");
                                break;
                            }
                            else
                            {
                                Console.Clear();
                                NumberOfAttempts++;
                                Console.WriteLine("Ошибка, попробуйте еще раз!");
                            }
                        }
                    }
                    catch (ApiException e)
                    {
                        Console.WriteLine($"Twilio Error {e.Code} - {e.MoreInfo}");
                    }
                    break;
                case "0":
                    break;
            }
                           
        }
    }
}
